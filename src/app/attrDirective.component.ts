import {Directive, ElementRef, Input, HostListener} from '@angular/core';

@Directive({
    selector : '[myHighlight]'
})

export class HighlightDirective{
    @Input('myHighlight') highlightColor: string;
    constructor(private el: ElementRef){
        el.nativeElement.style.backgroundColor = "yellow";
    }

    @HostListener('mouseenter') onMouseenter(){
        this.highlight(this.highlightColor || 'red');
    }

    @HostListener('mouseleave') onMouseleave(){
        this.highlight('blue');
    }

    highlight(color: string){
        this.el.nativeElement.style.backgroundColor = color;
    }
}