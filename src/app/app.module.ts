import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MyClass }  from './ngClass.component';
import { AnimationComponent }  from './animation.component';
import { HighlightDirective }  from './attrDirective.component';
import { AppComponent }  from './app.component';


@NgModule({
  imports: [ BrowserModule],
  declarations: [ AppComponent, MyClass, HighlightDirective, AnimationComponent ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }