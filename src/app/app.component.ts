import {Component} from '@angular/core';

@Component({
    selector : 'my-app',
    template :`<div class="container"><h1> Welcome to Angular 2. My App with webpack </h1>
    <my-class></my-class>
    <br>
    <div>
    <input type="radio" name="colors" (click)="color='lightgreen'">Green
    <input type="radio" name="colors" (click)="color='yellow'">Yellow
    <input type="radio" name="colors" (click)="color='cyan'">Cyan
    </div>
    <br>
    <p [myHighlight]="color">Hello I am Attribute directive </p>

    <p class="animation">Let's try animation in angular</p>
    <animate></animate>
    </div>
    ` ,

    styles: [`
        .animation{
            font-weight: bold;
            font-size : 2em;
        }
    `]
})

export class AppComponent{

}