import {Component} from '@angular/core';

@Component({
    selector: 'my-class',
    template: `<button class="btn-color" [class.extraclass]="someProperty">Click Me!!</button>
               <button class="btn-color" [ngClass]="someClass()">Click Me!!</button>
               <button class="btn-color" [ngStyle]="someStyle()">Click Me!!</button>
  
    `,
    styles:[`
        .extraclass{
            color: yellow;
        }
        .extraclass::after {
            content: 'Mouse Over here change content';
        }
        .extraclass:hover {
           color: blue;
        }
        .btn-color{
            background-color:red;
            border-radius:50px;
        }
        :host {
            display: block;
            border: 5px solid blue;
        }
    `]
})

export class MyClass{
    someProperty = true;
    someClass(){
        let classes = {
            extraclass: this.someProperty
        }

        return classes;
    }

    someStyle(){
        let styles = {
           'font-size' : this.someProperty ? '40px' : '70px'
        }

        return styles;
    }

}