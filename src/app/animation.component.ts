/*
Animations are defined in @Component metadata for that must import 
animation-specific functions.

- Trigger : it is used to Transit animation from one state to other state
define it as a propery bind [@triggername]
- State: An animation state is a string value, for ex : active and inactive are states.
- special state void applies when the element is not attached to a view, The void state 
is useful for defining enter and leave animations.
*/
import {Component, trigger, state, style, transition, animate, keyframes} from '@angular/core';

/*Transition between two states
- 
- 
*/

@Component({
    selector: 'animate',
    template:`
    <div class="row">
        <div class="col-md-4">
            <button class="btn btn-primary press" (click)="toggleMove()">Press me for animation</button>
        </div>
        <div class="col-md-4">
            <div id="content" [@focusPanel] = "state" [@flyInOut]= "state">Look at me animate</div>
        </div>
    </div>
    `,
    styles: [`
        .press{
            font-size:1.8em;
        }
        #content{
            padding:30px;
            background-color: #eeeeee;
        }
    `],

    animations: [
        trigger('focusPanel', [
            state('inactive', style({
                backgroundColor: "#aafeaf",
                 border : "3px solid red",
                transform: 'scale(1)'
            })),
            state('active', style({
                backgroundColor: "yellow",
                border : "3px solid black",
                transform: 'scale(1.2)'
            }))
           // transition('inactive => active', animate('100ms ease-in')),
           // transition('active => inactive', animate('100ms ease-out')),
            
        ]),
    
        trigger('flyInOut', [
            /*Multi-step animations with keyframes
            using keyframes, animation that goes through one or more
            intermediate styles when transitioning between two sets of styles.*/
    
            transition('void => inactive', [
                animate(5000, keyframes([
                style({opacity: 0, transform: 'translateX(-100%)', offset: 0}),
                style({opacity: 1, transform: 'translateX(15px)',  offset: 0.3}),
                style({opacity: 1, transform: 'translateX(0)',     offset: 1.0})
            ]))]),
            // transition('void => inactive', [
            // style({transform: 'translateX(-100px)'}),
            // animate(5000)
            // ]),
            transition('inactive => active', [
            animate(5000, style({transform: 'translateY(100px)'}))
            ]),
        ])

         ]

    

    
})

export class AnimationComponent{
    state: string = "inactive";
    toggleMove(){
       this.state =(this.state === 'inactive'? 'active' : 'inactive');
    }
}